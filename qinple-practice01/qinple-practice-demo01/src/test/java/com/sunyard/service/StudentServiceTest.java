package com.sunyard.service;

import com.sunyard.Demo01Application;
import com.sunyard.domain.pojo.Address;
import com.sunyard.domain.pojo.Student;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author qinple
 * @date 2021/6/16
 */
@SpringBootTest(classes = Demo01Application.class)
@RunWith(SpringRunner.class)
public class StudentServiceTest
{
    @Autowired
    private StudentService service;


    @Test
    public void testSaveInstance()
    {
        Student stu = new Student();
        stu.setId(new ObjectId());
        stu.setAge("23");
        stu.setName("张三");
        stu.setAddress(new Address("杭州", "302"));
        service.saveStudent(stu);
    }

    @Test
    public void testInsertInstance()
    {
        Student stu = new Student();
        ObjectId objectId = new ObjectId();
        String s = objectId.toString();
        System.out.println("s = " + s);
        stu.setId(objectId);
        stu.setAge("24");
        stu.setName("李四");
        stu.setAddress(new Address("浙江.杭州", "302"));
        service.insertStudent(stu);
    }

    @Test
    public void testFindInstance()
    {
        Student student = service.findStudent(new ObjectId("60c9a26240d7db3934ce3f9f"));
        System.out.println("student = " + student);
    }

    @Test
    public void testRemoveInstance()
    {
        String id = "60c9aa68a1228f3c645e6a4a";
        ObjectId objectId = new ObjectId(id);
        service.delStudentByObjectId(objectId);
    }

    @Test
    public void testFindAll()
    {
        List<Student> list = service.findList();
        System.out.println("list = " + list);
    }

    @Test
    public void testFindByObject()
    {
        List<Student> list = service.findByCondition();
        System.out.println("list = " + list);
    }
}
