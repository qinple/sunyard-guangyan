package com.sunyard.domain.pojo;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.StringJoiner;

/**
 * @author qinple
 * @date 2021/6/16
 */
@Document(collection = "tb_student")
public class Student implements Serializable
{
    @Transient
    private static final long serialVersionUID = -2480018125060267095L;

    @Id
    private ObjectId id;
    @Field("student_name")
    private String name;
    @Field("student_age")
    private String age;
    @Field("student_address")
    private Address address;

    public Student(ObjectId id, String name, String age, Address address)
    {
        this.id = id;
        this.name = name;
        this.age = age;
        this.address = address;
    }

    public Student()
    {

    }

    public ObjectId getId()
    {
        return id;
    }

    public void setId(ObjectId id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getAge()
    {
        return age;
    }

    public void setAge(String age)
    {
        this.age = age;
    }

    public Address getAddress()
    {
        return address;
    }

    public void setAddress(Address address)
    {
        this.address = address;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        Student student = (Student) o;

        if (id != null ? !id.equals(student.id) : student.id != null)
        {
            return false;
        }
        if (name != null ? !name.equals(student.name) : student.name != null)
        {
            return false;
        }
        if (age != null ? !age.equals(student.age) : student.age != null)
        {
            return false;
        }
        return address != null ? address.equals(student.address) : student.address == null;
    }

    @Override
    public int hashCode()
    {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (age != null ? age.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        return result;
    }

    @Override
    public String toString()
    {
        return new StringJoiner(", ", Student.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("age='" + age + "'")
                .add("address=" + address)
                .toString();
    }
}
