package com.sunyard.domain.pojo;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.StringJoiner;

/**
 * @author qinple
 * @date 2021/6/16
 */
public class Address implements Serializable
{
    @Transient
    private static final long serialVersionUID = 2012685121346280109L;

    @Field("local_name")
    private String localName;
    @Field("door_number")
    private String doorNumber;

    public Address()
    {
    }

    public Address(String localName, String doorNumber)
    {
        this.localName = localName;
        this.doorNumber = doorNumber;
    }

    public String getLocalName()
    {
        return localName;
    }

    public void setLocalName(String localName)
    {
        this.localName = localName;
    }

    public String getDoorNumber()
    {
        return doorNumber;
    }

    public void setDoorNumber(String doorNumber)
    {
        this.doorNumber = doorNumber;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        Address address = (Address) o;

        if (localName != null ? !localName.equals(address.localName) : address.localName != null)
        {
            return false;
        }
        return doorNumber != null ? doorNumber.equals(address.doorNumber) : address.doorNumber == null;
    }

    @Override
    public int hashCode()
    {
        int result = localName != null ? localName.hashCode() : 0;
        result = 31 * result + (doorNumber != null ? doorNumber.hashCode() : 0);
        return result;
    }

    @Override
    public String toString()
    {
        return new StringJoiner(", ", Address.class.getSimpleName() + "[", "]")
                .add("localName='" + localName + "'")
                .add("doorNumber='" + doorNumber + "'")
                .toString();
    }
}
