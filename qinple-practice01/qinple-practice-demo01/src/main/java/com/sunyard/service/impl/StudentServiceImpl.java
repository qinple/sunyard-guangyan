package com.sunyard.service.impl;

import com.sunyard.domain.pojo.Student;
import com.sunyard.service.StudentService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author qinple
 * @date 2021/6/16
 */
@Service
public class StudentServiceImpl implements StudentService
{
    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * save有更新的添加的作用,也有更新的作用; 取决于存入的id是否在文档中已经存在
     *
     * @param student 学生对象
     */
    @Override
    public void saveStudent(Student student)
    {
        //返回的结果是保存的对象本身
        Student result = mongoTemplate.save(student);
    }

    @Override
    public void insertStudent(Student student)
    {

        Student result = mongoTemplate.insert(student);
        System.out.println("result = " + result);
    }

    /**
     * 根据ObjectId删除记录
     *
     * @param id ObjectID
     */
    @Override
    public void delStudentByObjectId(ObjectId id)
    {
        //构造查询条件
        Query query = Query.query(Criteria.where("id").is(id));
        //删除操作
        mongoTemplate.remove(query, Student.class);
    }

    /**
     * 根据ObjectId查找单条记录
     * @param id ObjectId
     * @return 结果
     */
    @Override
    public Student findStudent(ObjectId id)
    {
        Query query = Query.query(Criteria.where("id").is(id));
        Student student = mongoTemplate.findOne(query, Student.class);
        return student;
    }

    @Override
    public List<Student> findList()
    {
        //第一个参数可以理解为查出的结果应该封装到怎样的对象中,第二个参数可以理解为表名()
        return mongoTemplate.findAll(Student.class, "tb_student");
    }

    @Override
    public List<Student> findByCondition()
    {
        Query query=Query.query(Criteria.where("id").ne(1));
        List<Student> list = mongoTemplate.find(query, Student.class, "tb_student");
        return list;
    }
}
