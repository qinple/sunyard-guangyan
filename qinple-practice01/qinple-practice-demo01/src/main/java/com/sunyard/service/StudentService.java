package com.sunyard.service;

import com.sunyard.domain.pojo.Student;
import org.bson.types.ObjectId;

import java.util.List;

/**
 * @author qinple
 * @date 2021/6/16
 */
public interface StudentService
{
    /**
     * 保存学生信息
     * @param student 学生对象
     */
    public abstract void saveStudent(Student student);

    /**
     * 保存学生信息二
     * @param student 学生对象
     */
    public abstract void insertStudent(Student student);

    /**
     * 根据ObjectID删除数据
     * @param id ObjectID
     */
    public abstract void delStudentByObjectId(ObjectId id);

    /**
     * 查单个学生
     * @param objectId id
     * @return 结果
     */
    public abstract Student findStudent(ObjectId objectId);

    /**
     * 查找所有学生信息
     * @return 装有学生信息的集合
     */
    public abstract List<Student> findList();

    /**
     * 根据条件查找
     * @return 装有学生信息的集合
     */
    public abstract List<Student> findByCondition();
}
