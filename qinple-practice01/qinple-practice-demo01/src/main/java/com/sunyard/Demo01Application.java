package com.sunyard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author qinple
 * @date 2021/6/16
 * 启动引导类
 */
@SpringBootApplication
public class Demo01Application
{
    public static void main(String[] args)
    {
        SpringApplication.run(Demo01Application.class, args);
    }
}
