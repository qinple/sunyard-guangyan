package com.sunyard.service;

import com.github.pagehelper.PageInfo;
import com.sunyard.domain.pojo.Goods;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author qinple
 * @date 2021/6/16
 */
@Transactional
public interface GoodsService
{
    /**
     * 保存商品信息
     *
     * @param goods 商品
     * @return 结果
     */
    boolean saveGoods(Goods goods);

    /**
     * 将查到的结果分页返回
     *
     * @return 结果
     * @param currentPage 当前页
     * @param pageSize 每页记录条数
     */
    PageInfo<Goods> findListAsPage(Integer currentPage, Integer pageSize);

    /**
     * 根据id查询商品信息
     * @param id 用户id
     * @return 结果
     */
    Goods findById(Long id);

    /**
     * 修改商品信息
     * @param goods 商品信息
     * @return 结果
     */
    boolean updateById(Goods goods);

    /**
     * 根据商品id删除数据
     * @param id 商品id
     * @return 结果
     */
    boolean deleteById(Long id);
}
