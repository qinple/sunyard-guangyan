package com.sunyard.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sunyard.domain.pojo.Goods;
import com.sunyard.mapper.GoodsMapper;
import com.sunyard.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author qinple
 * @date 2021/6/16
 */
@Service
public class GoodsServiceImpl implements GoodsService
{
    @Autowired
    private GoodsMapper mapper;

    @Override
    public boolean saveGoods(Goods goods)
    {
        //参数检测
        if (goods == null)
        {
            return false;
        }
        //保存操作,获得结果
        int result = mapper.save(goods);
        //根据影响行数判断是否成功
        return result == 1;
    }

    @Override
    public PageInfo<Goods> findListAsPage(Integer currentPage, Integer pageSize)
    {
        //参数校验
        if (currentPage == null || currentPage <= 0)
        {
            currentPage = 1;
        }
        if (pageSize == null || pageSize <= 0)
        {
            pageSize = 20;
        }
        //分页拦截
        PageHelper.startPage(currentPage, pageSize);
        //查询结果封装集合
        List<Goods> list = mapper.findAll();
        //创建分页对象,并返回分页信息
        return new PageInfo<>(list);
    }

    @Override
    public Goods findById(Long id)
    {
        if (id == null)
        {
            return null;
        }
        return mapper.findById(id);
    }

    @Override
    public boolean updateById(Goods goods)
    {
        //判断参数是否合法
        if (goods != null && goods.getId() != null)
        {
            //更新数据
            int result = mapper.updateById(goods);
            return result == 1;
        }
        return false;
    }

    @Override
    public boolean deleteById(Long id)
    {
        //参数校验
        if (id == null)
        {
            System.out.println("参数不合法");
            return false;
        }
        int result = mapper.delById(id);
        //执行方法,返回结果
        return result == 1;
    }
}
