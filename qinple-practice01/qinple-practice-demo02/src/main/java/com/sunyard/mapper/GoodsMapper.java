package com.sunyard.mapper;

import com.sunyard.domain.pojo.Goods;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author qinple
 * @Entity com.sunyard.domain.pojo.Goods
 */
@Mapper
@Repository
public interface GoodsMapper
{
    /**
     * 根据id删除记录
     * @param id 主键id
     * @return 影响行数
     */
    int delById(Long id);

    /**
     * 只插入非空的字段
     * @param goods 商品
     * @return 影响行数
     */
    int save(Goods goods);

    /**
     * 根据id找数据
     * @param id id
     * @return 结果
     */
    Goods findById(Long id);

    /**
     * 根据id修改商品信息
     * @param goods 商品
     * @return 影响行数
     */
    int updateById(Goods goods);

    /**
     * 查找出所有的商品信息
     * @return
     */
    List<Goods> findAll();



}




