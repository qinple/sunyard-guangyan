package com.sunyard.domain.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * @author qinple
 * @TableName goods
 */
public class Goods implements Serializable {

    private static final long serialVersionUID = 5605328305161078480L;
    /**
     * 
     */
    private Double id;

    /**
     * 
     */
    private String title;

    /**
     * 
     */
    private Long price;

    /**
     * 
     */
    private Double stock;

    /**
     * 
     */
    private Double saleNum;

    /**
     * 
     */
    private Date createTime;

    /**
     * 
     */
    private String categoryName;

    /**
     * 
     */
    private String brandName;

    /**
     * 
     */
    private String spec;


    /**
     * 
     */
    public Double getId() {
        return id;
    }

    /**
     * 
     */
    public void setId(Double id) {
        this.id = id;
    }

    /**
     * 
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     */
    public Long getPrice() {
        return price;
    }

    /**
     * 
     */
    public void setPrice(Long price) {
        this.price = price;
    }

    /**
     * 
     */
    public Double getStock() {
        return stock;
    }

    /**
     * 
     */
    public void setStock(Double stock) {
        this.stock = stock;
    }

    /**
     * 
     */
    public Double getSaleNum() {
        return saleNum;
    }

    /**
     * 
     */
    public void setSaleNum(Double saleNum) {
        this.saleNum = saleNum;
    }

    /**
     * 
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * 
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     * 
     */
    public String getBrandName() {
        return brandName;
    }

    /**
     * 
     */
    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    /**
     * 
     */
    public String getSpec() {
        return spec;
    }

    /**
     * 
     */
    public void setSpec(String spec) {
        this.spec = spec;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Goods other = (Goods) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getTitle() == null ? other.getTitle() == null : this.getTitle().equals(other.getTitle()))
            && (this.getPrice() == null ? other.getPrice() == null : this.getPrice().equals(other.getPrice()))
            && (this.getStock() == null ? other.getStock() == null : this.getStock().equals(other.getStock()))
            && (this.getSaleNum() == null ? other.getSaleNum() == null : this.getSaleNum().equals(other.getSaleNum()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getCategoryName() == null ? other.getCategoryName() == null : this.getCategoryName().equals(other.getCategoryName()))
            && (this.getBrandName() == null ? other.getBrandName() == null : this.getBrandName().equals(other.getBrandName()))
            && (this.getSpec() == null ? other.getSpec() == null : this.getSpec().equals(other.getSpec()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getTitle() == null) ? 0 : getTitle().hashCode());
        result = prime * result + ((getPrice() == null) ? 0 : getPrice().hashCode());
        result = prime * result + ((getStock() == null) ? 0 : getStock().hashCode());
        result = prime * result + ((getSaleNum() == null) ? 0 : getSaleNum().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getCategoryName() == null) ? 0 : getCategoryName().hashCode());
        result = prime * result + ((getBrandName() == null) ? 0 : getBrandName().hashCode());
        result = prime * result + ((getSpec() == null) ? 0 : getSpec().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", title=").append(title);
        sb.append(", price=").append(price);
        sb.append(", stock=").append(stock);
        sb.append(", saleNum=").append(saleNum);
        sb.append(", createTime=").append(createTime);
        sb.append(", categoryName=").append(categoryName);
        sb.append(", brandName=").append(brandName);
        sb.append(", spec=").append(spec);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}