package com.sunyard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author qinple
 * @date 2021/6/16
 */
@SpringBootApplication
public class Demo02Application
{
    public static void main(String[] args)
    {
        SpringApplication.run(Demo02Application.class, args);
    }
}
