package com.sunyard.service;

import com.github.pagehelper.PageInfo;
import com.sunyard.Demo02Application;
import com.sunyard.domain.pojo.Goods;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author qinple
 * @date 2021/6/17
 */
@SpringBootTest(classes = Demo02Application.class)
@RunWith(SpringRunner.class)
public class GoodsServiceTest
{
    @Autowired
    private GoodsService service;

    @Test
    public void testFindAll()
    {
        PageInfo<Goods> info = service.findListAsPage(0, 5);
        System.out.println("info = " + info);
    }

    @Test
    public void testFindOne()
    {
        Goods goods = service.findById(50000L);
        System.out.println("goods = " + goods);
    }
}
